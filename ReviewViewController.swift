//
//  ReviewViewController.swift
//  FoodPin
//
//  Created by Do Nguyen on 2/16/16.
//  Copyright © 2016 Zincer. All rights reserved.
//

import UIKit

class ReviewViewController: UIViewController {
    
    // MARK: Properties
    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var ratingStackView: UIStackView!
    @IBOutlet var badRating: UIButton!
    @IBOutlet var goodRating: UIButton!
    @IBOutlet var greatRating: UIButton!
    
    var rating: String?
    
    @IBAction func ratingSelected(sender: UIButton) {
        switch (sender.tag) {
        case 100: rating = "dislike"
        case 200: rating = "good"
        case 300: rating = "great"
        default: break
        }
        performSegueWithIdentifier("unwindToDetailView", sender: sender)
    }
    
    override func viewDidLoad() {
        // Add Blur Effect to the background
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.Dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        backgroundImageView.addSubview(blurEffectView)
    
        // Add Make scale Transformation to StackView
        let scale = CGAffineTransformMakeScale(0.0, 0.0)
        let translate = CGAffineTransformMakeTranslation(0, 500)

        
        badRating.transform = CGAffineTransformConcat(scale, translate)
        goodRating.transform = CGAffineTransformConcat(scale, translate)
        greatRating.transform = CGAffineTransformConcat(scale, translate)

    }
    
    override func viewDidAppear(animated: Bool) {
        // Dumping the rating images
        UIView.animateWithDuration(0.5, delay: 0.0, usingSpringWithDamping: 0.3,
            initialSpringVelocity: 0.5, options: [], animations: {
            self.badRating.transform = CGAffineTransformIdentity
            }, completion: nil)
        
        UIView.animateWithDuration(1.0, delay: 0.0, usingSpringWithDamping: 0.3,
            initialSpringVelocity: 0.5, options: [], animations: {
            self.goodRating.transform = CGAffineTransformIdentity
            }, completion: nil)
        
        UIView.animateWithDuration(1.5, delay: 0.0, usingSpringWithDamping: 0.3,
                initialSpringVelocity: 0.5, options: [], animations: {
            self.greatRating.transform = CGAffineTransformIdentity
                }, completion: nil)
        
    }
}
