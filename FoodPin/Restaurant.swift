//
//  Restaurant.swift
//  FoodPin
//
//  Created by Do Nguyen on 2/12/16.
//  Copyright © 2016 Zincer. All rights reserved.
//

import Foundation
import CoreData

class Restaurant: NSManagedObject {
    @NSManaged var name:String
    @NSManaged var type:String
    @NSManaged var location:String
    @NSManaged var image:NSData?
    @NSManaged var phoneNumber:String?
    @NSManaged var rating:String?
    @NSManaged var isVisited:NSNumber?
    
    func log() {
        print("Name: \(self.name) - Type: \(type) - Location: \(location) - Image: \(image) - Phone: \(phoneNumber) - Rating :\(rating)")
    }
}